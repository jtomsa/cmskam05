<?php
/**
 * @package     ${NAMESPACE}
 * @subpackage
 *
 * @copyright   A copyright
 * @license     A "Slug" license name e.g. GPL2
 */

//error_reporting(E_ALL);
//ini_set('display_errors', 'On');

require '../common/header.php';
require 'jt_connect.php';

$select_run = 'SELECT a.id_run, a.nrun, a.comment, a.run_comment, a.timestamp, b.run_type '
               . 'FROM run AS a INNER JOIN run_type AS b ON a.run_type_id = b.id_run_type '
               .'ORDER BY id_run ASC';
$result_run = $conn->query($select_run);


$html = '<table border="1"><thead>';
    $html .= '<tr>';
        $html .= '<td>Run №</td><td>Run type</td><td>Module</td><td>RBX</td><td>Slot</td><td>Timestamp</td><td>Comment</td>';
    $html .= '</tr></thead><tbody>';

    $all_files = scandir($_SERVER['DOCUMENT_ROOT'] . "/files/");
    unset($all_files[0]);
    unset($all_files[1]);
        while ($row = $result_run->fetch_array()) {
            $run_id = $row["id_run"];
            $select_run_setup = "SELECT mo.module_barcode, rb.rbx, sl.slot_rbx "
                                ."FROM run_setup AS rs INNER JOIN rbx AS rb ON rs.rbx_id = rb.id_rbx "
                                ."INNER JOIN module AS mo ON rs.module_id = mo.id_module "
                                ."INNER JOIN slot_rbx AS sl ON rs.slot_id = sl.id_slot_rbx "
                                ."WHERE rs.run_id = " . $run_id
                                ." ORDER BY id_run_setup ASC;";

            $run_file = "analysis_run_" . $row["nrun"] . ".root";
            $run_file_exists = file_exists($_SERVER['DOCUMENT_ROOT'] . "/files/" . $run_file);

            if ($run_file_exists == TRUE){
                if(($key = array_search($run_file, $all_files)) !== false) {
                    unset($all_files[$key]);
                }
            }

            $result_setup = $conn->query($select_run_setup);
            $row_count = $result_setup->num_rows;
            if ($row_count == 0){ $row_count = 1; }
            $FIRST_ITER = TRUE;
            while ($row_inner = $result_setup->fetch_array()){
                $html .= '<tr>';
                if ($FIRST_ITER == TRUE) {
                    $html .= '<td rowspan="' . $row_count . '">';
                    if ($run_file_exists == TRUE){$html .= '<a href="http://cmskam05.cern.ch/JsRoot460/index.htm?file=../files/' . $run_file . '" target="_blank">';}
                    $html .= $row["nrun"];
                    if ($run_file_exists == TRUE){$html .= '</a>';}
                    $html .= '</td><td rowspan="' . $row_count . '">' . $row["run_type"];
                    if ($row["run_comment"] != "") {
                        $html .= ' (' . $row["run_comment"] . ')';
                    }
                    $html .= "</td>";
                }
                $html .= '<td>'. $row_inner["module_barcode"] . '</td><td>'. $row_inner["rbx"] . '</td><td>'. $row_inner["slot_rbx"] . '</td>';

                if ($FIRST_ITER == TRUE) {
                    $html .= '<td rowspan="' . $row_count . '">' . $row["timestamp"] . '</td><td rowspan="' . $row_count . '">' . $row["comment"] . '</td>';
                    $FIRST_ITER = FALSE;
                }
                $html .= '</tr>';
            }
        }
$html .= '</tbody></table>';

$conn->close();

echo $html;

echo "<br><br> Unused Files: <br>";
foreach ($all_files as $unused){
    echo '<a href="http://cmskam05.cern.ch/JsRoot460/index.htm?file=../files/' . $unused . '" target="_blank">' . $unused . '</a><br>';
}

require '../common/footer.php';