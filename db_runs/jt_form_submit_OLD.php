<?php
require "../common/header.php";
require 'jt_connect.php';


$module_arr = $_POST['module'];
$rbx_arr = $_POST['rbx'];
$slot_arr = $_POST['slot'];
$setup_comment_arr = $_POST['setup_comment'];

$run_type = trim($_REQUEST['type']);
$nrun = trim($_REQUEST['nrun']);
$comment = trim($_REQUEST['comment']);
$run_comment = trim($_REQUEST['run_comment']);

$conn->autocommit(FALSE);
$insert_sql = "INSERT INTO run (nrun, comment, run_comment, run_type_id)" .
"VALUES('{$nrun}', '{$comment}', '{$run_comment}', '{$run_type}');";

if ($conn->query($insert_sql) === TRUE) {
    $last_id = $conn->insert_id;
} else {
    echo "<p style='color: red;'>Insertion error!!!" . $conn->error . "</p>";
    $conn->rollback();
    $conn->close();
    require "../common/footer.php";
    exit(0);
}

//echo "id of last run ";
//echo $last_id;
//echo "<br>";


if (!empty($rbx_arr)) {
    $insert_sql = "INSERT INTO run_setup (run_id, module_id, rbx_id, slot_id, comment) VALUES";
    $arr_ind = 0;
    foreach (array_keys($rbx_arr) as $key) {
        $insert_sql .= " ($last_id, $module_arr[$key], $rbx_arr[$key], $slot_arr[$key], '$setup_comment_arr[$key]'),";
    }
    $insert_sql = substr($insert_sql, 0, -1);
    $insert_sql .= ";";
//    echo $insert_sql;
    if ($conn->query($insert_sql) === TRUE) {
//        echo "<br> RBX numbers saved";
    } else {
        echo "<p style='color: red;'>Insertion error!!!" . $conn->error . "</p>";
        $conn->rollback();
        $conn->close();
        require "../common/footer.php";
        exit(0);
    }

} else {
    echo "No run setup selected";
}
$conn->commit();
$conn->close();

// moves root file from /data/import to files folder
$filename = "analysis_run_" . $nrun . ".root";
if (!rename("/data/import/" . $filename, $_SERVER['DOCUMENT_ROOT'] . "/files/" . $filename)) {
    echo "<p style='color: red;'> Moving root file " . $filename . " failed";
}

echo "<p>Run " . $nrun ." successfully added to database</p>";
//echo "<p>Edit this run</p>";
//echo "<p>Return to previous page</p>";

require "../common/footer.php";