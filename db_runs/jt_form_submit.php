<?php
require "../common/header.php";
require 'jt_connect.php';


$select_slot = 'SELECT id_slot_rbx, slot_rbx FROM slot_rbx ORDER BY slot_rbx ASC';
$result_slot = $conn->query($select_slot);

// an array mapping  slot number to it's database id
$slot_arr = array();
while ($row = $result_slot->fetch_array()){
    $slot_arr[$row["slot_rbx"]] = $row["id_slot_rbx"];
    //array_push($slot_arr, $row["slot_rbx"]);
}
//var_dump($slot_arr);
echo '<br><br>';

//var_dump($_POST);

$module_arr = $_POST['module'];
$rbx_arr = $_POST['rbx'];
//$slot_arr = $_POST['slot'];

$setup_comment_arr = $_POST['setup_comment'];

$run_type = trim($_REQUEST['type']);
$nrun = trim($_REQUEST['nrun']);
$comment = trim($_REQUEST['comment']);
$run_comment = trim($_REQUEST['run_comment']);

$conn->autocommit(FALSE);
$insert_sql = "INSERT INTO run (nrun, comment, run_comment, run_type_id)" .
"VALUES('{$nrun}', '{$comment}', '{$run_comment}', '{$run_type}');";

if ($conn->query($insert_sql) === TRUE) {
    $last_id = $conn->insert_id;
} else {
    echo "<p style='color: red;'>Insertion error!!!" . $conn->error . "</p>";
    $conn->rollback();
    $conn->close();
    require "../common/footer.php";
    exit(0);
}

if (!empty($rbx_arr)) {
    $insert_sql = "INSERT INTO run_setup (run_id, module_id, rbx_id, slot_id) VALUES";
//    $arr_ind = 0;
    foreach (array_keys($rbx_arr) as $key) {
        foreach (array_keys($slot_arr) as $slot) {
            $module_in = $_POST["module_in_" . $slot][$key];
            if ($module_in != "") {
                $insert_sql .= " (" . $last_id . ", " . $module_in . ", ";
                $insert_sql .= $rbx_arr[$key] . ", " . $slot_arr[$slot] . "),";
            }
        }
    }
    $insert_sql = substr($insert_sql, 0, -1);
    $insert_sql .= ";";
//    echo $insert_sql;
    if ($conn->query($insert_sql) === TRUE) {
//        echo "<br> RBX numbers saved";
    } else {
        echo "<p style='color: red;'>Insertion error!!!" . $conn->error . "</p>";
        $conn->rollback();
        $conn->close();
        require "../common/footer.php";
        exit(0);
    }

} else {
    echo "No run setup selected";
}
$conn->commit();
$conn->close();

// moves root file from /data/import to files folder
$filename = "analysis_run_" . $nrun . ".root";
if (!rename("/data/import/" . $filename, $_SERVER['DOCUMENT_ROOT'] . "/files/" . $filename)) {
    echo "<p style='color: red;'> Moving root file " . $filename . " failed";
}

echo "<p>Run " . $nrun ." successfully added to database</p>";
//echo "<p>Edit this run</p>";
//echo "<p>Return to previous page</p>";

require "../common/footer.php";