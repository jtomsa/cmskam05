<?php
/**
 * @package     ${NAMESPACE}
 * @subpackage
 *
 * @copyright   A copyright
 * @license     A "Slug" license name e.g. GPL2
 */

require '../common/header.php';
require 'jt_connect.php';

$select_run_type = 'SELECT id_run_type, run_type FROM run_type ORDER BY id_run_type ASC';
$result_run_type = $conn->query($select_run_type);

$select_module = 'SELECT id_module, module_barcode, description FROM module ORDER BY module_barcode ASC';
$result_module = $conn->query($select_module);

$select_rbx = 'SELECT id_rbx, rbx, description FROM rbx ORDER BY rbx ASC';
$result_rbx = $conn->query($select_rbx);

$select_slot = 'SELECT id_slot_rbx, slot_rbx, description FROM slot_rbx ORDER BY slot_rbx ASC';
$result_slot = $conn->query($select_slot);


//$html = '<form action="http://cmskam05.cern.ch/db_runs/jt_form_submit.php" method="post" id="main_run_form">';
//$html = '<form action="http://cmskam05.cern.ch/index.php?option=com_content&view=article&id=10" method="post" id="main_run_form">';
$html = '<form action="/db_runs/jt_form_submit.php" method="post" id="main_run_form">';
$html .= '<table><tbody>';
    $html .= '<tr>';
        $html .= '<td><label for="nrun">Run №: </label></td>';
        $html .= '<td><input name="nrun" size="30" type="number" min="0" step="1" pattern="\d+" required="required" value="' . $_GET['nrun'] . '"/></td>';
    $html .= '</tr>';
    $html .= '<tr>';
        $html .= '<td><label for="comment">Comment: </label></td>';
        $html .= '<td><input name="comment" type="text"/></td>';
    $html .= '</tr>';
    $html .= '<tr>';
        $html .= '<td><label for="type">Run type: </label></td>';
        $html .= '<td><select id="id_run_type_select"  required="required" name="type"/>';
            while ($row = $result_run_type->fetch_array()) {
                $html .= '<option value="' . $row["id_run_type"] . '">' . $row["run_type"] . '</option>';
            }
        $html .= '</select></td>';
    $html .= '</tr>';
    $html .= '<tr id="id_run_comment_row" class="hidden" >';
        $html .= '<td><label for="run_comment">Run type comment:</label></td>';
        $html .= '<td><textarea name="run_comment" rows="3"></textarea></td>';
    $html .= '</tr>';
$html .= '</tbody></table>';

$html .= '<table id="mod_rbx_slot_table"><thead>';
        $html .= '<tr><th>Module</th><th>RBX</th><th>Slot</th><th>Comment</th></tr>';
    $html .= '</thead><tbody>';
        $html .= '<tr id="row_template">';
            $html .= '<td><select  required="required" name="module[]" class="new_run_row">';
                while ($row = $result_module->fetch_array()) {
                    $html .= '<option value="' . $row["id_module"] . '">' . $row["module_barcode"] . ' (' . $row["description"] . ')</option>';
                }
            $html .= '</select>';

            $html .= '<td><select  required="required"  name="rbx[]" class="new_run_row">';
            while ($row = $result_rbx->fetch_array()) {
                $html .= '<option value="' . $row["id_rbx"] . '">' . $row["rbx"] . ' (' . $row["description"] . ')</option>';
            }
            $html .= '</select>';

            $html .= '<td><select  required="required" name="slot[]" class="new_run_row">';
            while ($row = $result_slot->fetch_array()) {
                $html .= '<option value="' . $row["id_slot_rbx"] . '">' . $row["slot_rbx"] . ' (' . $row["description"] . ')</option>';
            }
            $html .= '</select>';
            $html .= '<td><input name="setup_comment[]" type="text" class="new_run_row"/></td>';


        $html .= '</tr>';

$html .= '</tbody></table>';
$html .= '<input id="id_add_row_button" type="button" value="Add row"><br>';
$html .= '<input type="submit" value="Send data">';
$html .= '</form>';

echo $html;
require '../common/footer.php';
?>

