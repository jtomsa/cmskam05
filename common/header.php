<?php
/**
 * @package     ${NAMESPACE}
 * @subpackage
 *
 * @copyright   A copyright
 * @license     A "Slug" license name e.g. GPL2
 */

//error_reporting(E_ALL);
//ini_set('display_errors', 'On');

$header = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">';
$header .= '<HTML><HEAD><TITLE>CMSKAM05 database</TITLE>';
$header .= '<link rel="stylesheet" href="/static/stylesheets/css.css" type="text/css"/>';
$header .= '<script src="/static/javascript/jquery-3.1.0.js"></script>';
$header .= '<script type="text/javascript" src="/static/javascript/scripts.js"></script>';
$header .= '</HEAD><BODY>';
$header .= '<span class="top_menu"><a href="/db_runs/jt_new_run_form.php">New run</a></span>';
$header .= '<span class="top_menu"><a href="/db_runs/jt_runs_list.php">List of runs</a></span>';
$header .= '<hr>';
echo $header;