/**
 * @package     Joomla.Site
 * @subpackage  Templates.protostar
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @since       3.2
 */

(function($)
{
	$(document).ready(function()
	{
		$('*[rel=tooltip]').tooltip()

		// Turn radios into btn-group
		$('.radio.btn-group label').addClass('btn');

		$('fieldset.btn-group').each(function() {
			// Handle disabled, prevent clicks on the container, and add disabled style to each button
			if ($(this).prop('disabled')) {
				$(this).css('pointer-events', 'none').off('click');
				$(this).find('.btn').addClass('disabled');
			}
		});

		$(".btn-group label:not(.active)").click(function()
		{
			var label = $(this);
			var input = $('#' + label.attr('for'));

			if (!input.prop('checked')) {
				label.closest('.btn-group').find("label").removeClass('active btn-success btn-danger btn-primary');
				if (input.val() == '') {
					label.addClass('active btn-primary');
				} else if (input.val() == 0) {
					label.addClass('active btn-danger');
				} else {
					label.addClass('active btn-success');
				}
				input.prop('checked', true);
			}
		});
		$(".btn-group input[checked=checked]").each(function()
		{
			if ($(this).val() == '') {
				$("label[for=" + $(this).attr('id') + "]").addClass('active btn-primary');
			} else if ($(this).val() == 0) {
				$("label[for=" + $(this).attr('id') + "]").addClass('active btn-danger');
			} else {
				$("label[for=" + $(this).attr('id') + "]").addClass('active btn-success');
			}
		});

		/****
		 **  JT changes
		 **
		 *******/

		/* show run comment field */
		$("#id_run_type_select").change(function () {
			console.log($(this).find('option:selected').text());
			if ($.trim($(this).find('option:selected').text().toLowerCase()) == "other"){
				$("#id_run_comment_row").removeClass("hidden");
			} else {
				$("#id_run_comment_row").addClass("hidden");
			}
		});

        /* clone row in new run setup and adjust correctly names of inputs */
        $("#id_add_row_button").click(function () {
            var $clone = $("#row_template").clone();
            $clone.removeAttr('id');
            //console.log($("#mod_rbx_slot_table tbody tr").size());
            var new_index = $("#mod_rbx_slot_table tbody tr").size();
            // $clone.find("select[name=module_0]").attr('name', 'module_' + new_index).val("");
            // $clone.find("select[name=rbx_0]").attr('name', 'rbx_' + new_index).val("");
            // $clone.find("select[name=slot_0]").attr('name', 'slot_' + new_index).val("");
            // $clone.find("select[name=setup_comment_0]").attr('name', 'setup_comment_' + new_index).text("");

            $clone.find("select").val("");
            //$clone.find("select[name=rbx_0]").attr('name', 'rbx_' + new_index).val("");
            //$clone.find("select[name=slot_0]").attr('name', 'slot_' + new_index).val("");
            //console.log($clone.find("input"));
            $clone.find("input").val("");

            /* add a remove button */
            var $delete_button = $("<input>").attr('type', 'button')
                                             .attr('value', 'Remove')
                                             .click(function () {
                                                 $clone.remove();
                                             });
            var $delete_td = $("<td></td>").html($delete_button);
            $clone.append($delete_td);
            $("#mod_rbx_slot_table").find("tbody").append($clone);
        });
	})
})(jQuery);