/**
 * Created by janik on 22.9.16.
 */
// (function($) {
    $(document).ready(function()
    {
         /****
         **  JT changes
         **
         *******/
        /* show run comment field */
        $("#id_run_type_select").change(function () {
            console.log($(this).find('option:selected').text());
            if ($.trim($(this).find('option:selected').text().toLowerCase()) == "other"){
                $("#id_run_comment_row").removeClass("hidden");
            } else {
                $("#id_run_comment_row").addClass("hidden");
            }
        });

        /* clone row in new run setup and adjust correctly names of inputs */
        // + removing of currently removed
        $("#id_add_row_button").click(function () {
            var $clone = $("#row_template").clone();
            $clone.removeAttr('id');
            //console.log($("#mod_rbx_slot_table tbody tr").size());
            var new_index = $("#mod_rbx_slot_table tbody tr").size;
            // $clone.find("select[name=module_0]").attr('name', 'module_' + new_index).val("");
            // $clone.find("select[name=rbx_0]").attr('name', 'rbx_' + new_index).val("");
            // $clone.find("select[name=slot_0]").attr('name', 'slot_' + new_index).val("");
            // $clone.find("select[name=setup_comment_0]").attr('name', 'setup_comment_' + new_index).text("");

            $clone.find("select").val("");
            //$clone.find("select[name=rbx_0]").attr('name', 'rbx_' + new_index).val("");
            //$clone.find("select[name=slot_0]").attr('name', 'slot_' + new_index).val("");
            //console.log($clone.find("input"));
            $clone.find("input").val("");

            /* add a remove button */
            var $delete_button = $("<input>").attr('type', 'button')
                .attr('value', 'Remove')
                .addClass('remove_button');
                // .click(function () {
                //     //$clone.remove();
                // })
            var $delete_td = $("<td></td>").html($delete_button);
            $clone.append($delete_td);
            $("#mod_rbx_slot_table").find("tbody").append($clone);
        });

        // deleting of all rows - both pregenerated and post generated using jquery
        $("#mod_rbx_slot_table").delegate(".remove_button", "click", function () {
           $(this).closest("tr").remove();
        });



    });
// })();