-- MySQL dump 10.13  Distrib 5.6.33, for Linux (x86_64)
--
-- Host: localhost    Database: jt_runs
-- ------------------------------------------------------
-- Server version	5.6.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `module`
--

DROP TABLE IF EXISTS `module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module` (
  `id_module` int(11) NOT NULL AUTO_INCREMENT,
  `module_barcode` int(40) DEFAULT NULL,
  `module_hw_code` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `module_type_id` int(11) NOT NULL,
  `description` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_module`),
  UNIQUE KEY `module_code_UNIQUE` (`module_barcode`),
  UNIQUE KEY `module_hw_id_UNIQUE` (`module_hw_code`),
  KEY `fk_module_type_idx` (`module_type_id`),
  CONSTRAINT `fk_module_type` FOREIGN KEY (`module_type_id`) REFERENCES `module_type` (`id_module_type`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module`
--

LOCK TABLES `module` WRITE;
/*!40000 ALTER TABLE `module` DISABLE KEYS */;
/*!40000 ALTER TABLE `module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_type`
--

DROP TABLE IF EXISTS `module_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_type` (
  `id_module_type` int(11) NOT NULL AUTO_INCREMENT,
  `module_type` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_module_type`),
  UNIQUE KEY `module_type_UNIQUE` (`module_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_type`
--

LOCK TABLES `module_type` WRITE;
/*!40000 ALTER TABLE `module_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `position_rbx`
--

DROP TABLE IF EXISTS `position_rbx`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `position_rbx` (
  `id_position_rbx` int(11) NOT NULL AUTO_INCREMENT,
  `position_rbx` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_position_rbx`),
  UNIQUE KEY `position_rbx_UNIQUE` (`position_rbx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `position_rbx`
--

LOCK TABLES `position_rbx` WRITE;
/*!40000 ALTER TABLE `position_rbx` DISABLE KEYS */;
/*!40000 ALTER TABLE `position_rbx` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rbx`
--

DROP TABLE IF EXISTS `rbx`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rbx` (
  `id_rbx` int(11) NOT NULL AUTO_INCREMENT,
  `rbx` int(11) DEFAULT NULL,
  `description` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_rbx`),
  UNIQUE KEY `rbx_UNIQUE` (`rbx`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rbx`
--

LOCK TABLES `rbx` WRITE;
/*!40000 ALTER TABLE `rbx` DISABLE KEYS */;
INSERT INTO `rbx` VALUES (1,1,NULL),(2,2,NULL),(3,3,NULL),(4,4,NULL),(5,5,NULL),(6,6,NULL),(7,7,NULL),(8,8,NULL),(9,9,NULL),(10,10,NULL),(11,11,NULL),(12,12,NULL),(13,13,NULL),(14,14,NULL),(15,15,NULL),(16,16,NULL),(17,17,NULL),(18,18,NULL),(19,19,NULL);
/*!40000 ALTER TABLE `rbx` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `run`
--

DROP TABLE IF EXISTS `run`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `run` (
  `id_run` int(11) NOT NULL AUTO_INCREMENT,
  `nrun` int(20) NOT NULL,
  `comment` varchar(100) DEFAULT NULL,
  `run_comment` varchar(100) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `run_type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_run`),
  UNIQUE KEY `nrun_UNIQUE` (`nrun`),
  KEY `fk_run_1_idx` (`run_type_id`),
  CONSTRAINT `fk_run_type` FOREIGN KEY (`run_type_id`) REFERENCES `run_type` (`id_run_type`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `run`
--

LOCK TABLES `run` WRITE;
/*!40000 ALTER TABLE `run` DISABLE KEYS */;
/*!40000 ALTER TABLE `run` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `run_setup`
--

DROP TABLE IF EXISTS `run_setup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `run_setup` (
  `id_run_setup` int(11) NOT NULL AUTO_INCREMENT,
  `run_id` int(11) NOT NULL,
  `rbx_id` int(11) NOT NULL,
  `module_id` int(11) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_run_setup`),
  UNIQUE KEY `all_unique` (`rbx_id`,`run_id`,`module_id`,`position_id`),
  KEY `fk_run_idx` (`run_id`),
  KEY `fk_rbx_idx` (`rbx_id`),
  KEY `fk_module_idx` (`module_id`),
  KEY `fk_setup_position_idx` (`position_id`),
  CONSTRAINT `fk_setup_module` FOREIGN KEY (`module_id`) REFERENCES `module` (`id_module`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_setup_position` FOREIGN KEY (`position_id`) REFERENCES `position_rbx` (`id_position_rbx`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_setup_rbx` FOREIGN KEY (`rbx_id`) REFERENCES `rbx` (`id_rbx`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_setup_run` FOREIGN KEY (`run_id`) REFERENCES `run` (`id_run`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `run_setup`
--

LOCK TABLES `run_setup` WRITE;
/*!40000 ALTER TABLE `run_setup` DISABLE KEYS */;
/*!40000 ALTER TABLE `run_setup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `run_type`
--

DROP TABLE IF EXISTS `run_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `run_type` (
  `id_run_type` int(11) NOT NULL AUTO_INCREMENT,
  `run_type` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `comment` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_run_type`),
  UNIQUE KEY `run_type_UNIQUE` (`run_type`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `run_type`
--

LOCK TABLES `run_type` WRITE;
/*!40000 ALTER TABLE `run_type` DISABLE KEYS */;
INSERT INTO `run_type` VALUES (1,'G50',NULL),(2,'V72',NULL),(3,'other',NULL);
/*!40000 ALTER TABLE `run_type` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-09-16 16:09:13
