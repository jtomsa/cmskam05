-- MySQL dump 10.13  Distrib 5.6.33, for Linux (x86_64)
--
-- Host: localhost    Database: jt_runs
-- ------------------------------------------------------
-- Server version	5.6.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `module`
--

DROP TABLE IF EXISTS `module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module` (
  `id_module` int(11) NOT NULL AUTO_INCREMENT,
  `module_barcode` int(40) DEFAULT NULL,
  `module_hw_code` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `module_type_id` int(11) NOT NULL,
  `description` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_module`),
  UNIQUE KEY `module_code_UNIQUE` (`module_barcode`),
  UNIQUE KEY `module_hw_id_UNIQUE` (`module_hw_code`),
  KEY `fk_module_type_idx` (`module_type_id`),
  CONSTRAINT `fk_module_type` FOREIGN KEY (`module_type_id`) REFERENCES `module_type` (`id_module_type`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module`
--

LOCK TABLES `module` WRITE;
/*!40000 ALTER TABLE `module` DISABLE KEYS */;
INSERT INTO `module` VALUES (1,1,NULL,1,'mod #1'),(2,12,NULL,1,'mod #2'),(3,13,'',2,'mod#3');
/*!40000 ALTER TABLE `module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_type`
--

DROP TABLE IF EXISTS `module_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_type` (
  `id_module_type` int(11) NOT NULL AUTO_INCREMENT,
  `module_type` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_module_type`),
  UNIQUE KEY `module_type_UNIQUE` (`module_type`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_type`
--

LOCK TABLES `module_type` WRITE;
/*!40000 ALTER TABLE `module_type` DISABLE KEYS */;
INSERT INTO `module_type` VALUES (1,'typ1','prvni'),(2,'typ2','druhy');
/*!40000 ALTER TABLE `module_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rbx`
--

DROP TABLE IF EXISTS `rbx`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rbx` (
  `id_rbx` int(11) NOT NULL AUTO_INCREMENT,
  `rbx` int(11) DEFAULT NULL,
  `description` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_rbx`),
  UNIQUE KEY `rbx_UNIQUE` (`rbx`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rbx`
--

LOCK TABLES `rbx` WRITE;
/*!40000 ALTER TABLE `rbx` DISABLE KEYS */;
INSERT INTO `rbx` VALUES (1,0,'RBX0'),(2,1,'RBX1'),(3,2,'RBX2'),(4,3,'RBX3'),(5,4,'RBX4'),(6,5,'RBX5'),(7,6,'RBX6'),(8,7,'RBX7'),(9,8,'RBX8'),(10,9,'RBX9'),(11,10,'RBX10'),(12,11,'RBX11'),(13,12,'RBX12'),(14,13,'RBX13'),(15,14,'RBX14'),(16,15,'RBX15'),(17,16,'RBX16'),(18,17,'RBX17'),(19,18,'RBX18'),(20,19,'RBX19');
/*!40000 ALTER TABLE `rbx` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `run`
--

DROP TABLE IF EXISTS `run`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `run` (
  `id_run` int(11) NOT NULL AUTO_INCREMENT,
  `nrun` int(20) NOT NULL,
  `comment` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `run_comment` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `run_type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_run`),
  UNIQUE KEY `nrun_UNIQUE` (`nrun`),
  KEY `fk_run_1_idx` (`run_type_id`),
  CONSTRAINT `fk_run_type` FOREIGN KEY (`run_type_id`) REFERENCES `run_type` (`id_run_type`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `run`
--

LOCK TABLES `run` WRITE;
/*!40000 ALTER TABLE `run` DISABLE KEYS */;
INSERT INTO `run` VALUES (52,1000022535,'ghf','','2016-09-21 16:02:05',1),(56,211231,'','','2016-09-21 21:06:02',1),(58,1000022536,'p','','2016-09-21 21:10:00',1),(62,1000022537,'','tyrty','2016-09-22 12:06:22',3),(80,454554,'','','2016-09-22 15:07:46',1);
/*!40000 ALTER TABLE `run` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `run_setup`
--

DROP TABLE IF EXISTS `run_setup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `run_setup` (
  `id_run_setup` int(11) NOT NULL AUTO_INCREMENT,
  `run_id` int(11) NOT NULL,
  `rbx_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `slot_id` int(11) NOT NULL,
  `comment` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_run_setup`),
  UNIQUE KEY `all_unique` (`rbx_id`,`run_id`,`module_id`,`slot_id`),
  KEY `fk_run_idx` (`run_id`),
  KEY `fk_rbx_idx` (`rbx_id`),
  KEY `fk_module_idx` (`module_id`),
  KEY `fk_setup_position_idx` (`slot_id`),
  CONSTRAINT `fk_setup_module` FOREIGN KEY (`module_id`) REFERENCES `module` (`id_module`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_setup_position` FOREIGN KEY (`slot_id`) REFERENCES `slot_rbx` (`id_slot_rbx`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_setup_rbx` FOREIGN KEY (`rbx_id`) REFERENCES `rbx` (`id_rbx`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_setup_run` FOREIGN KEY (`run_id`) REFERENCES `run` (`id_run`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `run_setup`
--

LOCK TABLES `run_setup` WRITE;
/*!40000 ALTER TABLE `run_setup` DISABLE KEYS */;
INSERT INTO `run_setup` VALUES (20,52,1,1,1,''),(23,56,1,1,1,''),(24,58,1,1,1,''),(25,58,4,3,2,'sddsd'),(28,62,1,1,1,'qwer'),(29,62,8,3,3,'wetr'),(35,80,1,2,1,NULL);
/*!40000 ALTER TABLE `run_setup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `run_type`
--

DROP TABLE IF EXISTS `run_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `run_type` (
  `id_run_type` int(11) NOT NULL AUTO_INCREMENT,
  `run_type` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `comment` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_run_type`),
  UNIQUE KEY `run_type_UNIQUE` (`run_type`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `run_type`
--

LOCK TABLES `run_type` WRITE;
/*!40000 ALTER TABLE `run_type` DISABLE KEYS */;
INSERT INTO `run_type` VALUES (1,'PED',NULL),(2,'LED',NULL),(3,'other',NULL);
/*!40000 ALTER TABLE `run_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slot_rbx`
--

DROP TABLE IF EXISTS `slot_rbx`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slot_rbx` (
  `id_slot_rbx` int(11) NOT NULL AUTO_INCREMENT,
  `slot_rbx` int(11) NOT NULL,
  `description` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_slot_rbx`),
  UNIQUE KEY `position_rbx_UNIQUE` (`slot_rbx`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slot_rbx`
--

LOCK TABLES `slot_rbx` WRITE;
/*!40000 ALTER TABLE `slot_rbx` DISABLE KEYS */;
INSERT INTO `slot_rbx` VALUES (1,1,'RM1'),(2,2,'RM2'),(3,3,'RM3'),(4,4,'RM4'),(5,5,'ngCCM'),(6,6,'CM');
/*!40000 ALTER TABLE `slot_rbx` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-09-22 17:13:04
